import { bundle } from "https://deno.land/x/emit@0.0.2/mod.ts";
import { createHash } from "https://deno.land/std@0.80.0/hash/mod.ts";
export async function bundleAndSave() {
    await grabMDL_URLs();
    await bundleMain();
    await bundleSW();
}

async function grabMDL_URLs() {
    const grep = /'https:\/\/cdn.skypack.dev\/.*'/g;
    const file = await Deno.readTextFile("./public/mdl.js");
    let s = "export const mdlImps = [\n";
    for(const lnk of file.matchAll(grep)){
        s = s + lnk[0] + ",\n";
    }
    s = s + "]";
    await Deno.writeTextFile("./src/mdl_autogen.ts", s);
}

async function bundleMain() {
    const url = new URL("./src/main/main.ts", import.meta.url);
    const emited = await bundle(url.href);
    await Deno.writeTextFile("./public/main.js", emited.code);
    const hash = createHash("md5").update(emited.code).toString();
    const shortHash = hash.substring(hash.length - 5);
    console.log(`Bundled hash ${shortHash}`)
    await Deno.writeTextFile("./public/hash", shortHash);
}

async function bundleSW() {
    const url = new URL("./src/service-worker/sw.ts", import.meta.url);
    const emited = await bundle(url.href);
    await Deno.writeTextFile("./public/sw.js", emited.code);
}

if (import.meta.main) {
    console.log("Bundeling...")
    await bundleAndSave();
    console.log("Bundled.")
}