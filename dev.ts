#!/usr/bin/env -S deno run -A
import { bundleAndSave } from "./build.ts"
try {
    await bundleAndSave();
} catch (_e) {
    console.warn("Error preforming bundle!");
}

const watcher = Deno.watchFs("./src/");
top:for await (const event of watcher) {
    for(const path of event.paths){
        if(path.endsWith("mdl_autogen.ts")){
            continue top;
        }
    }
    if (event.kind == "modify" || event.kind == "remove" || event.kind == "create") {
        try {
            await bundleAndSave();
        } catch (e) {
            console.warn("Error preforming bundle!");
            console.log(e)
        }
    }
}