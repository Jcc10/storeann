/// <reference lib="dom" />
import {UI} from "./ui.ts";
import {Nagg} from "./nagg.ts";
import {VoiceManager} from "./voice-manager.ts"
import {TranslateEngine} from "./translate-engine.ts"

import {BasePage} from "./pages/base.ts";
import {HomePage} from "./pages/home.ts";
import {InstallPage} from "./pages/misc/install.ts";
import {SharePage} from "./pages/misc/share.ts";
import {DiagnosticPage} from "./pages/misc/diagnostic.ts";
import {StaticPage} from "./pages/static.ts"
import {HoursSettings} from "./pages/settings/hours.ts"
import {StoreSettings} from "./pages/settings/store.ts"
import {SpeechSettingsPage} from "./pages/settings/speech.ts"
import {HoursPage} from "./pages/hours.ts"
import {BaseAnnouncer} from "./pages/announcers/base.ts";

// Initialize deferredPrompt for use later to show browser install prompt.
let deferredPrompt;

const ui = new UI();
const _nagg = new Nagg();
const voiceManager = new VoiceManager();
//const translationEngine = new TranslateEngine();


const pages = new Map<string, BasePage>();
//pages.set("diag", new DiagnosticPage())
// HOME
pages.set("home", new HomePage(()=>{ui.pageSwitchCB("closing", 2)}))
pages.set("hours", new HoursPage())
// STATIC PAGES
pages.set("share", new SharePage())
pages.set("about", new StaticPage("misc/about", "about"))
pages.set("help", new StaticPage("misc/help", "help"))
// INSTALL PAGE
const installPage = new InstallPage(deferredPrompt)
pages.set("install", installPage);
globalThis.addEventListener('beforeinstallprompt', (e) => {
    // Prevent the mini-infobar from appearing on mobile
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    installPage.setInstallEvent(deferredPrompt);
    // Optionally, send analytics event that PWA install promo was shown.
    console.log(`'beforeinstallprompt' event was fired.`);
  });


// Announcers
pages.set("closing", new BaseAnnouncer("closing", "closing announcer Test"))
pages.set("manual", new StaticPage("announcers/manual", "manual announcer"))
// Settings
pages.set("setHours", new HoursSettings())
pages.set("storeVars", new StoreSettings())
pages.set("TTS", new SpeechSettingsPage(voiceManager))
pages.set("translation", new StaticPage("settings/translation", "translation settings"))

ui.pageSwitchCB = async (pageName: string, index: number) => {
    console.log(`Trying for ${pageName}`)
    ui.appPageList.select(index);
    if(pages.has(pageName)){
        console.log(`Found ${pageName}`)
        const page = pages.get(pageName);
        if(!page){
            return;
        }
        await page.ready;
        const title = page.swapTo();
        if(title && ui.appTitlebar){
            ui.appTitlebar.innerText = title;
        }
    }
}

declare global {
    interface Window {
        pageSwitch: (page: string) => void;
    }
}
window.pageSwitch = (pageName: string) => {
    ui.pageSwitchCB(pageName, 0);
}

installPage.loadHash()
{
    await ui.pageSwitchCB("TTS", 0)
    ui.removeSplash();
}

function networkUpdate() {
    const lineStat = document.getElementById("lineStat") as HTMLSpanElement;
    if(!lineStat){
        return
    }
    if(navigator.onLine){
        lineStat.innerText = "signal_wifi_4_bar"
    } else {
        lineStat.innerText = "signal_wifi_connected_no_internet_4"
    }
}
networkUpdate();
globalThis.addEventListener("online", networkUpdate);
globalThis.addEventListener("offline", networkUpdate);

const devSite = document.getElementById("devStat") as HTMLSpanElement;
if(devSite){
    if(
        globalThis.location.href.includes("wwjdtd.net") ||
        globalThis.location.href.includes("jcc10.net")
        ){
            devSite.hidden = false;
            devSite.style.display = ""
            // If on dev domain, check every 30 seconds.
            setInterval(() => {
                installPage.loadHash()
            }, 30 *1000 )
    } else {
        // If on main domain, check every 1.5 hours (for now).
        setInterval(() => {
            installPage.loadHash()
        }, 1.5 * 60 *1000)
    }   
}

const isStandalone = window.matchMedia('(display-mode: standalone)').matches;
if(isStandalone){
    const list = ui.appPageList as HTMLDivElement;
    for(let i = 0; i < list.children.length; i++){
        const elem = list.children[i] as HTMLInputElement;
        if(elem.value == "install"){
            elem.children[0].innerHTML = "App Update Control"
            elem.children[1].innerHTML = "upgrade"
        }
    }
}


if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register("./sw.js").then(function (registration) {
        console.log(
          "ServiceWorker registration successful with scope: ",
          registration.scope,
        );
      }, function (err) {
        console.log("ServiceWorker registration failed: ", err);
      });
  }