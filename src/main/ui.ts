/// <reference lib="dom" />
import type {
    materialMwcIconButton,
    materialMwcList,
    materialMwcDrawer,
} from "./mdc.d.ts";

export class UI {
    appDrawer?:materialMwcDrawer;
    appPageList?: materialMwcList;
    appTitlebar?: HTMLDivElement;
    public pageSwitchCB = (page: string, index: number):void|Promise<void> => {
        console.log(`default page switcher going to ${page} | ${index}`)
    };
    constructor() {
        this.appDrawer = document.getElementsByTagName('mwc-drawer')[0] as materialMwcDrawer;
        this.appPageList = document.getElementById("appNavigator") as materialMwcList;
        this.appTitlebar = document.getElementById("title") as HTMLDivElement;
        this.setupBurgerDrawer();
        this.setupAppNavigator();
    }
    removeSplash() {
        const splash = document.getElementById("splash") as HTMLDivElement;
        if(splash){
            const anim = splash.animate([
                {top: 0, borderBottom: "solid var(--mdc-theme-primary) 0.75vh"},
                {top:"-101vh", borderBottom: "solid var(--mdc-theme-primary) 0.75vh", offset: 0.99},
                {top:"-101vh", borderBottom: "solid var(--mdc-theme-primary) 0.0"}
            ], {
                fill:"forwards",
                duration: 750,
                easing:"ease-out",
            });
            anim.onfinish = ()=>{
                splash.parentElement?.removeChild(splash);
            }
        }
    }
    private setupBurgerDrawer() {
        const burger = document.querySelectorAll("mwc-icon-button[icon='menu'][slot='navigationIcon']")[0] as materialMwcIconButton;
        if(this.appDrawer && burger){
            burger.addEventListener("click", () => {
                this.appDrawer.open = !this.appDrawer.open
            })
        }
    }
    private setupAppNavigator() {
        if(this.appPageList && this.appDrawer){
            this.appPageList.addEventListener("action", () => {
                this.appDrawer.open = false;
                this.pageSwitchCB(this.appPageList.selected.value, this.appPageList.index)
            })
        }
    }
}