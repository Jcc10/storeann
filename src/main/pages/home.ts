/// <reference lib="dom" />
import { BasePage } from "./base.ts";
import { AppStorage } from "../appStorage.ts"
import { getDay, DayHours } from "../week.ts";

export class HomePage extends BasePage {
    nextClose = [23, 59];
    gtcann;
    constructor(goToCloseAnn: ()=>void) {
        super("home", "Home");
        this.gtcann = goToCloseAnn;
    }
    hydrate() {
        const goToCloseAnnButton = document.getElementById("goToCloseAnn") as HTMLButtonElement;
        if(goToCloseAnnButton){
            goToCloseAnnButton.addEventListener("click", ()=>{
                this.gtcann();
            });
        }
        const day = new Date().getDay();
        const today =  getDay(day, true, "full").toString();
        this.nextClose = AppStorage.getData<DayHours>(`${today}-hours`)[0]?.close || [23, 59];
        globalThis.requestAnimationFrame(()=>{this.tick()});
    }
    tick() {
        const timeToClose = document.getElementById("timeToClose") as HTMLSpanElement;
        if(!timeToClose){
            return;
        }
        const now = new Date();
        const nowHour = now.getHours();
        const nowMinute = now.getMinutes();
        const deltaHour = this.nextClose[0] - nowHour;
        const deltaMinutePartial = this.nextClose[1] - nowMinute;
        const deltaMinutes = deltaMinutePartial + (deltaHour * 60);
        const nowSec = now.getSeconds();
        timeToClose.innerText = `${Math.floor(deltaMinutes/60)}:${(Math.abs((deltaMinutes%60)-1)+"").padStart(2, "0")}:${(59-nowSec+"").padStart(2, "0")}`
        globalThis.requestAnimationFrame(()=>{this.tick()});
    }
}