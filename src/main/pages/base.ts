/// <reference lib="dom" />
import { deferred } from "https://deno.land/std@0.146.0/async/mod.ts";
export abstract class BasePage {
    protected pageName: string;
    protected main: HTMLElement;
    protected body: string | null = null;
    private __ready = deferred<void>();
    constructor(pageURL: string, pageName: string){
        this.pageName = pageName;
        this.main = document.getElementsByTagName('main')[0] as HTMLElement;
        fetch(`./pages/${pageURL}.html`)
        .then(resp=>resp.text())
        .then(body=>{
            this.body = body;
            this._ready();
        });
    }
    protected _ready(){
        this.__ready.resolve();
    }
    public get ready() {
        return this.__ready;
    }
    swapTo(){
        console.debug(`Swapped To ${this.pageName}`);
        this.addToPage();
        this.hydrate();
        return this.pageName;
    }
    protected addToPage(){
        if(this.body){
            this.main.innerHTML = this.body;
        }
    }
    protected abstract hydrate(): void;
}