/// <reference lib="dom" />
import type { materialMwcDialog, materialMwcSlider } from "../../mdc.d.ts"
import { BasePage } from "../base.ts";
import { AppStorage } from "../../appStorage.ts"
import { getDay, DayHours } from "../../week.ts";

const replacerRexExp = /\${(.+?)}/g;
const ttsSplitRegExp = /(.+?\.)/g;

export class BaseAnnouncer extends BasePage {

    protected repVar = new Map<string, string>();
    protected synth = window.speechSynthesis;
    protected segment = 0;
    protected segments?: RegExpMatchArray[];
    protected stop = true;

    constructor(announcerURL: string, announcerTitle: string) {
        super(`announcers/${announcerURL}`, announcerTitle);
    }
    protected hydrate(): void {
        const fab = document.getElementsByTagName("mwc-fab")[0];
        const dialouge = document.getElementsByTagName("mwc-dialog")[0] as materialMwcDialog;
        const ppButton = document.getElementById("primary-action-button") as HTMLButtonElement;
        if(fab && dialouge){
            fab.addEventListener("click", () => {
                this.openAnnWindow();
            });
            ppButton.addEventListener("click", () => {
                this.ppTrigger();
            })
            dialouge.addEventListener("closing", () => {
                this.pause();
            })
        }
        const timeRemaining = document.getElementById("timeRemaining") as materialMwcSlider;
        if(timeRemaining) {
            timeRemaining.addEventListener("input", () => {
                this.announcerFormatter();
            })
        }
    }

    protected openAnnWindow(): void {
        const dialouge = document.getElementsByTagName("mwc-dialog")[0] as materialMwcDialog;
        const ppButton = document.getElementById("primary-action-button") as HTMLButtonElement;
        const timeRemaining = document.getElementById("timeRemaining") as materialMwcSlider;
        this.updateVars();
        const closing = AppStorage.getData<DayHours>(`${this.repVar.get("today")}-hours`)[0]?.close
        if(closing && timeRemaining){
            const now = new Date();
            const nowHour = now.getHours();
            const nowMinute = now.getMinutes();
            const deltaHour = closing[0] - nowHour;
            const deltaMinutePartial = closing[1] - nowMinute;
            const deltaMinute = deltaMinutePartial + (deltaHour * 60);
            if(deltaMinute <= 0){
                timeRemaining.value = 0
            } else if(deltaMinute > 60) {
                timeRemaining.value = 65;
            } else {
                timeRemaining.value = Math.round(deltaMinute / 5) * 5;
            }
        }
        this.announcerFormatter();
        this.synth.cancel();
        dialouge.show();
        ppButton.innerText = "Play";
    }

    protected ppTrigger(): void {
        const ppButton = document.getElementById("primary-action-button") as HTMLButtonElement;
        if(this.synth.speaking) {
            this.pause();
            ppButton.innerText = "Resume";
        } else {
            this.play();
            ppButton.innerText = "Pause";
        }
    }

    protected play(): void {
        this.stop = false;
        const annReader = document.getElementById("annReader") as HTMLTextAreaElement;
        if(!annReader?.value){
            return;
        }
        this.segments = [...annReader.value.matchAll(ttsSplitRegExp)];
        this.resume();
    }

    protected resume(): void {
        if(!this.segments){
            this.pause();
            return
        }
        if(this.segments.length <= this.segment){
            return
        }
        const seg = this.segments[this.segment]
        const u = new SpeechSynthesisUtterance(seg[1]);
        const annReader = document.getElementById("annReader") as HTMLTextAreaElement;
        const start = seg.index || 0;
        const end = ((seg.index || 0) + seg[0].length) || annReader.value.length;
        annReader.focus();
        annReader.setSelectionRange(start, end);
        u.onend = () => {
            if(this.stop){
                return;
            }
            this.segment++;
            this.resume();
        }
        this.synth.speak(u);
    }

    protected pause(): void {
        this.stop = true;
        this.synth.cancel();
    }

    protected announcerFormatter(): void {
        const annForm = document.getElementById("annForm") as HTMLTextAreaElement;
        const annReader = document.getElementById("annReader") as HTMLTextAreaElement;
        this.updateInteractiveVars();
        const replacedString = this.replacer(annForm.value);
        annReader.value = replacedString;
        this.segment = 0;
        annReader.setSelectionRange(0,0)
    }
    protected updateVars(): void {
        const day = new Date().getDay();
        this.repVar.set("today", getDay(day, true, "full").toString());
        for(let i=1; i < 7; i++){
            const nextName = getDay(day + i, true, "full").toString()
            const nextData = AppStorage.getData<DayHours>(`${nextName}-hours`)[0] || {close:[0, 0], open: [0,0], closed: true};
            if(!nextData){
                continue;
            }
            if(nextData.closed == true){
                continue;
            }
            this.repVar.set("tomorow", nextName);
            if(i == 1){
                this.repVar.set("nextDayWord", "tomorow")
            } else {
                //this.repVar.set("nextDayWord", `in ${i} days`)
                this.repVar.set("nextDayWord", `on ${nextName}`)
            }
            if(nextData?.open){
                const timeObj = new Date();
                timeObj.setHours(nextData.open[0], nextData.open[1]);
                let timeWords = "birght and early";
                if(timeObj.getMinutes() == 0){
                    timeWords = timeObj.toLocaleTimeString('en-US', {
                        // en-US can be set to 'default' to use user's browser settings
                        hour: '2-digit',
                    })
                } else {
                    timeWords = timeObj.toLocaleTimeString('en-US', {
                        // en-US can be set to 'default' to use user's browser settings
                        hour: '2-digit',
                        minute: '2-digit',
                    })
                }
                this.repVar.set("nextOpen", "at " + timeWords);
            } else {
                this.repVar.set("nextOpen", "birght and early");
            }
            break;
        }
        this.repVar.set("store", AppStorage.getData<string>("storeName")[0] || "Our Store");
        this.repVar.set("time", "shortly");
    }
    protected updateInteractiveVars(): void {
        const timeRemaining = document.getElementById("timeRemaining") as materialMwcSlider;
        if(timeRemaining?.value !== undefined){
            console.log(timeRemaining.value)
            if(timeRemaining.value > 60){
                this.repVar.set("time", `in over 1 hour`);
            } else if(timeRemaining.value == 60){
                this.repVar.set("time", `in 1 hour`);
            } else if(timeRemaining.value == 0){
                this.repVar.set("time", `momentarily`);
            } else {
                this.repVar.set("time", `in ${timeRemaining.value} minutes`);
            }
        }
    }
    protected replacer(inStr: string): string {
        let s = inStr;
        for(const repSet of s.matchAll(replacerRexExp)){
            const stripped = repSet[1];
            const target = repSet[0]
            let rep = this.repVar.get(stripped);
            if(!rep){
                rep = stripped;
            }
            s = s.replace(target, rep);
        }
        return s;
    }
}