/// <reference lib="dom" />
import { BasePage } from "../base.ts";
import { materialMwcSnackbar } from "../../mdc.d.ts"
import { AppStorage } from "../../appStorage.ts"
import { appCacheName, filesToCache } from "../../../cache-files.ts";

//TODO: Hash stuff should become a backround sync task.

export class InstallPage extends BasePage {
    protected installEvent: {prompt: ()=>void};
    protected lastServerHash = "Offline";
    protected lastHash = "First Launch";
    protected updateSnackbar?: materialMwcSnackbar;
    constructor(installEvent: unknown) {
        super("misc/install", "install app");
        this.installEvent = installEvent as {prompt: ()=>void};
        this.lastHash = AppStorage.getData<string>("savedHash")[0] || "First Launch";
        {
            this.updateSnackbar = document.getElementById("updateSnackbar") as materialMwcSnackbar;
            const updateSnackbarReloadBtn = this.updateSnackbar?.querySelector("#updateSnkButton") as HTMLButtonElement;
            if(this.updateSnackbar){
                this.updateSnackbar.timeoutMs = -1;
            }
            if(updateSnackbarReloadBtn){
                updateSnackbarReloadBtn.addEventListener("click", () => {
                    this.update();
                })
            }
        }
    }

    public loadHash() {
        if(navigator.onLine){
            fetch("./hash").then(res=>res.text()).then(text=>{
                console.log("Hash is", text);
                this.updateHash(text);
            });
        }
    }

    hydrate() {
        this.loadHash();
        const installButton = document.getElementById("install") as HTMLButtonElement;
        const refreshButton = document.getElementById("refresh") as HTMLButtonElement;
        if(!this.installEvent){
            installButton.disabled = true;
            console.log("Disabled install!")
        }
        if(installButton){
            installButton.addEventListener("click", ()=>{
                this.install();
            });
        }
        if(refreshButton){
            refreshButton.addEventListener("click", ()=>{
                this.update();
            });
        }


        const savedHash = document.getElementById("savedHash") as HTMLSpanElement;
        const currentHash = document.getElementById("currentHash") as HTMLSpanElement;
        if(savedHash && currentHash){
            savedHash.innerText = this.lastHash;
            currentHash.innerHTML = this.lastServerHash
        }
    }

    public setInstallEvent(installEvent: unknown) {
        this.installEvent = installEvent as {prompt: ()=>void};
    }

    protected install() {
        if(this.installEvent){
            this.installEvent.prompt();
        } else {
            console.log("Can't install!")
        }
    }
    protected update() {
        globalThis.location.reload();
    }

    public async updateHash(newHash: string | null) {
        let lastHash = AppStorage.getData<string>("savedHash")[0] || "First Launch";
        if(newHash !== null){
            if(lastHash != newHash && lastHash != "First Launch"){
                const cache = await caches.open(appCacheName);
                cache.addAll(filesToCache)
                AppStorage.setData("savedHash", newHash);
                lastHash = newHash;
                this.lastServerHash = newHash;
                const currentHash = document.getElementById("currentHash") as HTMLSpanElement;
                if(currentHash){
                    currentHash.innerHTML = this.lastServerHash
                }
                this.updateSnackbar?.show();
            }
            if(lastHash == "First Launch"){
                AppStorage.setData("savedHash", newHash);
                lastHash = newHash;
            }
            this.lastServerHash = newHash
        }
    }
}