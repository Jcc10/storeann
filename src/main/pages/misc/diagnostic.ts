/// <reference lib="dom" />
import { BasePage } from "../base.ts";
import type { materialMwcDialog, materialMwcSlider } from "../../mdc.d.ts"

import {TranslateEngine} from "../../translate-engine.ts";

export class DiagnosticPage extends BasePage {
    engine: TranslateEngine;
    constructor() {
        super("misc/diagnostic", "diagnostic");
        this.engine =new TranslateEngine();
    }
    hydrate() {
        window.engine = this.engine;
    }
}

declare global {
    interface Window {
        engine: TranslateEngine;
    }
}