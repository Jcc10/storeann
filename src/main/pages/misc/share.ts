/// <reference lib="dom" />
import { BasePage } from "../base.ts";
import { materialMwcSnackbar } from "../../mdc.d.ts"
//import { AppStorage } from "../../appStorage.ts"

export class SharePage extends BasePage {
    constructor() {
        super("misc/share", "share the app!");
    }
    hydrate() {
        const shareButton = document.getElementById("share") as HTMLButtonElement;
        const shareErrorSnackbar = document.getElementById("shareErrorSnackbar") as materialMwcSnackbar
        if(shareButton){
            shareButton.addEventListener("click", () => {
                const shareData: ShareData = {
                    title: "Store Announcer",
                    text: "Play announcements from your phone.",
                    url: window.location.href,
                }
                if(!navigator.canShare || !navigator.share){
                    shareErrorSnackbar.show();
                    return;
                }
                if(navigator.canShare(shareData)){
                    navigator.share(shareData);
                }
            })
        }
    }
}