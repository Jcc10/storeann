/// <reference lib="dom" />
import {BasePage} from "../base.ts";
import {VoiceManager, Voice, ExtendedVoice} from "../../voice-manager.ts"
import type { materialMwcDialog, materialMwcSelect, materialMwcSnackbar } from "../../mdc.d.ts"

// To futue people, the `fixSynthSelects` spam is because it wasn't working and may still not be working.

export class SpeechSettingsPage extends BasePage {
    vm: VoiceManager
    currentVoice: Voice | null = null;
    constructor(voiceManager: VoiceManager) {
        super("settings/speech", "Edit Voices");
        this.vm = voiceManager;
    }
    hydrate(): void {
        this.reloadVoices();
        const synthSelect = document.getElementById("synths") as materialMwcSelect;
        synthSelect.addEventListener("change", () => {this.updateLC()})
        const saveVoice = document.getElementById("primary-action-button") as HTMLButtonElement;
        saveVoice.addEventListener("click", () => {
            this.saveVoice();
        })
        const deleteVoice = document.getElementById("deleteVoice") as HTMLButtonElement;
        deleteVoice.addEventListener("click", () => {
            this.deleteVoice();
        })
        const testVoice = document.getElementById("sayTestSentence") as HTMLButtonElement;
        testVoice.addEventListener("click", () => {
            this.testVoice();
        })
        
        const testVoiceSnack = document.getElementById("testSpeech") as materialMwcSnackbar;
        testVoiceSnack.timeoutMs = -1;
        const TempFix1 = document.getElementById("TempFix1") as HTMLButtonElement;
        TempFix1.addEventListener("click", () => {
            this.fixSynthSelects();
        })
    }
    reloadVoices(): void {
        const voiceList = document.getElementById("voices") as HTMLElement;
        voiceList.innerHTML = "";
        for(const voice of this.vm.getVoices()){
            const listElem = document.createElement("mwc-list-item");
            const nameSpan = document.createElement("span");
            nameSpan.innerText = voice.name
            const langSpan = document.createElement("span");
            langSpan.innerText = ` (${voice.lang})`
            listElem.appendChild(nameSpan)
            listElem.appendChild(langSpan)
            nameSpan.dataset.voice = voice.name;
            listElem.dataset.voice = voice.name;
            langSpan.dataset.voice = voice.name;
            listElem.addEventListener("click", (e) => {
                const li = e.target as HTMLElement;
                const name = li.dataset.voice;
                const voice = this.vm.getVoice(name);
                this.showDialouge(voice)
            })
            voiceList?.appendChild(listElem);
        }
        {
            const listElem = document.createElement("mwc-list-item");
            const nameSpan = document.createElement("span");
            nameSpan.innerText = "New Voice"
            const langSpan = document.createElement("span");
            listElem.appendChild(nameSpan)
            listElem.appendChild(langSpan)
            listElem.addEventListener("click", (_e) => {
                this.showDialouge(null)
                this.fixSynthSelects();
            })
            voiceList?.appendChild(listElem);
        }
    }
    showDialouge(voice: ExtendedVoice | null) {
        this.currentVoice = voice;
        const dialog = document.getElementById("dialog") as materialMwcDialog;
        console.log(voice);
        const name = document.getElementById("name") as HTMLInputElement;
        name.value = voice?.name || "New Voice";
        name.readOnly = voice?.default || false;
        const deleteVoice = document.getElementById("deleteVoice") as HTMLButtonElement;
        deleteVoice.disabled = voice?.default || false;
        const synthSelect = document.getElementById("synths") as materialMwcSelect;
        synthSelect.disabled = true;
        const TempFix1 = document.getElementById("TempFix1") as HTMLButtonElement;
        TempFix1.style.display = ""
        synthSelect.innerHTML = `<mwc-list-item selected></mwc-list-item>`
        for(const synth of globalThis.speechSynthesis.getVoices()){
            const nameSpan = document.createElement("mwc-list-item") as HTMLOptionElement;
            nameSpan.innerText = synth.name;
            nameSpan.value = synth.name;
            synthSelect.appendChild(nameSpan);
        }
        this.updateLC();
        dialog.show();
    }
    private fixSynthSelects() {
        const synthSelect = document.getElementById("synths") as materialMwcSelect;
        synthSelect.selectByValue(this.currentVoice?.synth);
        synthSelect.reportValidity();
        synthSelect.disabled = false;
        const TempFix1 = document.getElementById("TempFix1") as HTMLButtonElement;
        TempFix1.style.display = "none"
    }
    private updateLC() {
        const langCode = document.getElementById("langCode") as HTMLInputElement;
        const synthSelect = document.getElementById("synths") as materialMwcSelect;
        const synthName = synthSelect.value;
        langCode.value = ``;
        for(const voice of globalThis.speechSynthesis.getVoices()){
            if(voice.name == synthName){
                langCode.value = `${voice.lang} (${voice.localService ? "local" : "online"})`;
                break;
            }
        }
    }
    private generateVoiceFromDialouge() {

        const name = document.getElementById("name") as HTMLInputElement;
        const synthSelect = document.getElementById("synths") as materialMwcSelect;
        const synthName = synthSelect.value;
        if(!synthName){
            synthSelect.reportValidity();
            return;
        }
        let langCode = "";
        for(const voice of globalThis.speechSynthesis.getVoices()){
            if(voice.name == synthName){
                langCode = voice.lang;
                break;
            }
        }
        const voice: Voice = {
            name: name.value,
            default: this.currentVoice?.default || false,
            local: false,
            synth: synthName,
            lang: langCode,
        }
        return voice
    }

    private saveVoice() {
        const voice = this.generateVoiceFromDialouge();
        if(!voice){
            return;
        }
        this.vm.setVoice(voice);
        this.vm.saveVoices();
        this.reloadVoices();
        const dialog = document.getElementById("dialog") as materialMwcDialog;
        dialog.close();
    }

    deleteVoice() {
        const name = document.getElementById("name") as HTMLInputElement;
        this.vm.deleteVoice(name.value);
        this.vm.saveVoices();
        this.reloadVoices();
        const dialog = document.getElementById("dialog") as materialMwcDialog;
        dialog.close();
    }

    testVoice() {
        const voice = this.generateVoiceFromDialouge();
        if(!voice){
            return;
        }
        const utter = this.vm.utterWithVoice(`This is a test of ${voice.name} with synth ${voice.synth}`, voice);
        if(!utter){
            return;
        }

        const testVoiceSnack = document.getElementById("testSpeech") as materialMwcSnackbar;
        testVoiceSnack.labelText = `Testing voice ${utter.voice?.name}`;
        testVoiceSnack.show();
        utter.onend = () => {
            testVoiceSnack.close();
        }
        globalThis.speechSynthesis.cancel();
        globalThis.speechSynthesis.speak(utter);
        globalThis.speechSynthesis.resume();
        console.log("Testing Voice")
    }
}