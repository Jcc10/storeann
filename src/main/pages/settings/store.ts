/// <reference lib="dom" />
import { materialMwcTextfield, materialMwcSnackbar } from "../../mdc.d.ts"
import { BasePage } from "../base.ts";
import { AppStorage } from "../../appStorage.ts"

export class StoreSettings extends BasePage {
    constructor() {
        super("settings/store", "Store Settings");
    }
    hydrate() {
        const savedSnackbar = document.getElementById("savedSnackbar") as materialMwcSnackbar
        const saveErrorSnackbar = document.getElementById("saveErrorSnackbar") as materialMwcSnackbar
        if(savedSnackbar && saveErrorSnackbar){
            savedSnackbar.timeoutMs = 4000
            saveErrorSnackbar.timeoutMs = 4000
        }
        const storeName = document.getElementById("storeName") as materialMwcTextfield;
        if(storeName){
            storeName.value = AppStorage.getData<string>("storeName")[0] || "Our Store"
        }
        const saveButton = document.getElementById("save") as HTMLButtonElement;
        if(saveButton){
            saveButton.addEventListener("click", ()=>{
                this.save();
            });
        }
    }
    save(): void {
        let error = false;
        const storeName = document.getElementById("storeName") as materialMwcTextfield
        if(storeName){
            AppStorage.setData("storeName", storeName.value);
        } else {
            error = true;
        }

        if(!error){
            const savedSnackbar = document.getElementById("savedSnackbar") as materialMwcSnackbar
            if(savedSnackbar){
                savedSnackbar.show();
            }
        } else {
            const saveErrorSnackbar = document.getElementById("saveErrorSnackbar") as materialMwcSnackbar
            if(saveErrorSnackbar){
                saveErrorSnackbar.show();
            }
        }
    }
}