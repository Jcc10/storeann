/// <reference lib="dom" />
import { materialMwcSelect, materialMwcTextfield, materialMwcCheckbox } from "../../mdc.d.ts"
import { HoursPage } from "../hours.ts";
import { AppStorage } from "../../appStorage.ts"
import { DayHours } from "../../week.ts";

const timeParser = /(\d\d):(\d\d)/;

//TODO: Make this a extension of the hours viewer.

export class HoursSettings extends HoursPage {
    constructor() {
        super("settings/", "set hours");
    }
    hydrate() {
        this.hydrateWeekStart();
        this.initialHydrateHours();
        this.hydrateHoliday();
    }
    protected hydrateWeekStart() {
        this.startOnSunday = (AppStorage.getData<[boolean]>("startOnSunday")[0] || [false])[0];
        const selElm = document.getElementById("startOn") as materialMwcSelect;
        selElm.addEventListener("selected", (event: Event) => {
            const target = event.target as materialMwcSelect;
            if(target.index == 0){
                this.startOnSunday = true;
            } else if(target.index == 1) {
                this.startOnSunday = false;
            }
            AppStorage.setData("startOnSunday", [this.startOnSunday])
            this.hydrateHours("div");
        })
        if(this.startOnSunday){
            selElm.children[0].selected = true;
        } else {
            selElm.children[1].selected = true;
        }
    }
    private initialHydrateHours() {
        const schedule = document.querySelectorAll("main #normal > div");
        for(let i = 0; i < schedule.length; i++){
            const open = (schedule[i].children[1] as HTMLSpanElement).children[0] as materialMwcTextfield;
            const close = (schedule[i].children[2] as HTMLSpanElement).children[0] as materialMwcTextfield;
            const hardClosed = (schedule[i].children[3] as HTMLSpanElement).children[0].children[0] as materialMwcCheckbox;
            open.addEventListener("change", (event: Event) => {
                const elem = event.target as materialMwcTextfield
                const value = elem.value as string;
                const parsed = value.match(timeParser);
                if(!parsed){
                    return;
                }
                const day = elem.dataset.day;
                const dayHours = AppStorage.getData<DayHours>(`${day}-hours`)[0] || {open: [9, 0], close: [17, 0]};
                dayHours.open = [parseInt(parsed[1]), parseInt(parsed[2])];
                AppStorage.setData(`${day}-hours`, dayHours);
            })
            close.addEventListener("change", (event: Event) => {
                const elem = event.target as materialMwcTextfield
                const value = elem.value as string;
                const parsed = value.match(timeParser);
                if(!parsed){
                    return;
                }
                const day = elem.dataset.day;
                const dayHours = AppStorage.getData<DayHours>(`${day}-hours`)[0] || {open: [9, 0], close: [17, 0]};
                dayHours.close = [parseInt(parsed[1]), parseInt(parsed[2])];
                AppStorage.setData(`${day}-hours`, dayHours);
            })
            hardClosed.addEventListener("change", (event: Event) => {
                const elem = event.target as materialMwcCheckbox
                const day = elem.dataset.day;
                const dayHours = AppStorage.getData<DayHours>(`${day}-hours`)[0] || {open: [9, 0], close: [17, 0]};
                dayHours.closed = elem.checked;
                AppStorage.setData(`${day}-hours`, dayHours);
                this.hydrateHours("div");
            })
        }
        this.hydrateHours("div");
    }
    protected hydrateHoliday(){

    }
}