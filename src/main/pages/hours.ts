/// <reference lib="dom" />
import { materialMwcTextfield, materialMwcCheckbox } from "../mdc.d.ts"
import { BasePage } from "./base.ts";
import { AppStorage } from "../appStorage.ts"
import { getDay, DayHours } from "../week.ts";

export class HoursPage extends BasePage {
    protected startOnSunday = false;
    constructor(prefix = "", title="Store Hours") {
        super(`${prefix}hours`, title);
    }
    hydrate() {
        this.hydrateWeekStart();
        this.hydrateHours(undefined, true);
        this.hydrateHoliday();
    }
    protected hydrateWeekStart() {
        this.startOnSunday = (AppStorage.getData<[boolean]>("startOnSunday")[0] || [false])[0];
    }
    protected hydrateHours(target = "mwc-list-item", activate=false) {
        const schedule = document.querySelectorAll(`main #normal > ${target}`);
        let today = new Date().getDay() + 7;
        if(!this.startOnSunday){
            today = today - 1
        }
        today = (today % 7)
        for(let i = 0; i < schedule.length; i++){
            const fullDay = getDay(i, this.startOnSunday, "full")
            const name = schedule[i].children[0] as HTMLSpanElement;
            name.innerText = fullDay + "";
            const dayHours = AppStorage.getData<DayHours>(`${fullDay}-hours`)[0] || {open: [9, 0], close: [17, 0]};
            const open = (schedule[i].children[1] as HTMLSpanElement).children[0] as materialMwcTextfield;
            open.value = `${(dayHours.open[0]+"").padStart(2, "0")}:${(dayHours.open[1]+"").padStart(2, "0")}`
            open.dataset.day = fullDay
            const close = (schedule[i].children[2] as HTMLSpanElement).children[0] as materialMwcTextfield;
            close.value = `${(dayHours.close[0]+"").padStart(2, "0")}:${(dayHours.close[1]+"").padStart(2, "0")}`
            close.dataset.day = fullDay
            const hardClosed = (schedule[i].children[3] as HTMLSpanElement)?.children[0]?.children[0] as materialMwcCheckbox;
            if(hardClosed){
                hardClosed.checked = dayHours.closed;
                hardClosed.dataset.day = fullDay
            }
            if(dayHours.closed){
                open.value = ""
                open.disabled = true;
                close.value = ""
                close.disabled = true;
            } else {
                open.disabled = false;
                close.disabled = false;
            }
            if(activate){
                if(i == today){
                    schedule[i].setAttribute("activated", "activated");
                }
            }
        }
    }
    protected hydrateHoliday(){

    }
}