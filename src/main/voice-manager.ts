import { AppStorage } from "./appStorage.ts"

export interface Voice {
    name: string,
    default: boolean,
    local: boolean,
    synth: string,
    lang?: string,
    pitch?: number,
    rate?: number,
    volume?: number,
}
export interface ExtendedVoice extends Voice {
    voiceObject: SpeechSynthesisVoice | null;
}

export class VoiceManager {
    private _voices: Voice[];
    constructor() {
        const voices = AppStorage.getData<Voice[]>("voices")[0];
        this._voices = [];
        if(!voices || voices.length == 0){
            this.onVoicesChange();
        } else {
            this._voices = voices;
        }
        globalThis.speechSynthesis.addEventListener("voiceschanged", ()=>this.onVoicesChange());
    }
    onVoicesChange() {
        this.voiceNullCheck();
    }
    voiceNullCheck() {
        if(this._voices.length == 0){
            const voice = VoiceManager.generateDefaultVoice();
            if(!voice){
                return;
            }
            const defaultList = [voice];
            this._voices = defaultList;
        }
    }
    static generateDefaultVoice() {
        const voices = globalThis.speechSynthesis.getVoices();
        let defaultIntrnalVoice: SpeechSynthesisVoice | null = null;
        for(const voice of voices){
            defaultIntrnalVoice = voice;
            if(voice.default){
                defaultIntrnalVoice = voice;
                break;
            }
        }
        if(defaultIntrnalVoice){
            const defaultVoice: Voice = {
                name: "default",
                default: true,
                local: defaultIntrnalVoice.localService || false,
                synth: defaultIntrnalVoice.name,
                lang: defaultIntrnalVoice.lang
            }
            return defaultVoice;
        }
        return null;
    }
    saveVoices() {
        AppStorage.setData("voices", this._voices);
    }
    getVoices() {
        const voices = this._voices;
        const fullVoices: ExtendedVoice[] = []
        for(const voice of voices){
            const exVoice = this.ensureExtendedVoice(voice, globalThis.speechSynthesis.getVoices());
            if(exVoice){
                fullVoices.push(exVoice);
            }
        }
        return fullVoices;
    }
    getVoice(name: string | undefined){
        if(!name){
            return null;
        }
        for(const Cvoice of this._voices){
            if(Cvoice.name == name){
                return this.ensureExtendedVoice(Cvoice);
            }
        }
        return null;
    }
    setVoice(voice: Voice) {
        for(const i in this._voices){
            if(this._voices[i].name == voice.name){
                this._voices[i] = voice;
                return;
            }
        }
        this._voices.push(voice);
    }
    deleteVoice(name: string) {
        for(const i in this._voices){
            if(this._voices[i].name == name){
                const index = parseInt(i);
                this._voices.splice(index, 1)
                return;
            }
        }
    }
    ensureExtendedVoice(voice: Voice | ExtendedVoice, cachedVoices?: SpeechSynthesisVoice[]): ExtendedVoice | null{
        if((voice as ExtendedVoice).voiceObject){
            return {...voice} as ExtendedVoice;
        }
        if(!cachedVoices){
            cachedVoices = globalThis.speechSynthesis.getVoices();
        }
        const extendedVoice: ExtendedVoice = {...voice, voiceObject: null};
        for(const synth of cachedVoices){
            if(extendedVoice.synth == synth.name){
                extendedVoice.voiceObject = synth
                return extendedVoice;
            }
        }
        return null;
    }
    utterWithVoice( text: string, voiceParam: Voice | ExtendedVoice){
        const voice = this.ensureExtendedVoice(voiceParam);
        if(!voice){
            return;
        }
        const u = new SpeechSynthesisUtterance(text);
        // Chrome Android breaks with this.
        /*if(voice?.voiceObject){
            u.voice = voice?.voiceObject;
        }*/
        if(!u.voice){
            u.lang = voice.lang || u.lang;
        }
        u.pitch = voice.pitch || 1;
        u.rate = voice.rate || 1;
        u.volume = voice.volume || 1;
        return u;
    }
}