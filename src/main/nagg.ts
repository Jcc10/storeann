import { AppStorage } from "./appStorage.ts"

export class Nagg {
    constructor() {
        this.COS_FirstRun();
        const rc = this.CAS_RunCount();
        this.LoadNagDialouge(rc);
    }
    COS_FirstRun() {
        const asfr = AppStorage.getData<string>("FirstRun")[0];
        if(!asfr){
            const today = new Date();
            today.setHours(0, 0, 0, 0);
            AppStorage.setData("FirstRun", today);
            AppStorage.setData("LastRun", today);
        }
    }
    CAS_RunCount() {
        const aslr = AppStorage.getData<string>("LastRun")[0];
        let rc = AppStorage.getData<number>("RunCount")[0] || 0;
        if(aslr){
            const dlr = new Date(aslr);
            const today = new Date();
            today.setHours(0, 0, 0, 0);
            if(
                dlr.getDate() != today.getDate() ||
                dlr.getMonth() != today.getMonth() ||
                dlr.getFullYear() != today.getFullYear()
                ) {
                rc++
                AppStorage.setData("RunCount", rc);
            }
            AppStorage.setData("LastRun", today);
        }
        return rc;
    }
    LoadNagDialouge(rc: number) {
        if(rc % 30){
            console.log("Donation Request Goes Here");
        }
    }
}