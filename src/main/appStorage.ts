export class AppStorage {
    static getData<t>(itemKey: string): [t | undefined, "found" | "missing" | "error"] {
        const item = window.localStorage.getItem(itemKey);
        if(!item){
            return [undefined, "missing"];
        }
        try{
            const parsed = JSON.parse(item);
            return [parsed, "found"];
        } catch(_e) {
            return [undefined, "error"];
        }
    }
    // deno-lint-ignore no-explicit-any
    static setData(itemKey: string, data: any) {
        window.localStorage.setItem(itemKey, JSON.stringify(data));
    }
    static delete(itemKey: string) {
        window.localStorage.removeItem(itemKey);
    }
    static deleteAll() {
        window.localStorage.clear();
    }
}