/// <reference lib="dom" />
import { deferred } from "https://deno.land/std@0.146.0/async/mod.ts";
export interface ModelRegester {
    "lex": unknown;
    "model": unknown;
    "vocab": unknown;
}
export type ModelRegistry = Record<string, ModelRegester>;
interface translateOptions {
    isQualityScores: boolean,
    isHtml: boolean,
}

export class TranslateEngine {
    worker?: Worker;
    modelRegistry?: ModelRegistry;
    langOpts?: string[];
    inLang?: string;
    lastStatus = "";
    nextStatus = deferred<string>();
    lastTranslate?: string[];
    nextTranslate = deferred<string[]>();
    loadedModels = new Array<string>();
    constructor() {
        this.worker = new Worker(" bergamot/translator-worker-wrapper.js");
        console.log("Starting Translate Engine...");
        this.worker.onmessage = (e: MessageEvent<[string, ...unknown[]]>) => {
            this.workerMessageRouter(e)
        }
        this.worker.postMessage(["import"]);
    }

    async translate(inText: string, inLangCode: string, outLangCode: string, html = false){
        if(!this.loadedModels.includes(`${inLangCode} ${outLangCode}`)){
            await this.loadModel(inLangCode, outLangCode);
        }
        const text = inText;
        if (!text.trim().length) return;
        let paragraphs: string[] = text.split(/\n+/); 
        if(html){
            paragraphs = paragraphs.map(this.textToHTML); // escape HTML 
        }
        const translateOptions = this.prepareTranslateOptions(paragraphs, html, false);
        const lngFrom = inLangCode;
        const lngTo = outLangCode;
        const nextTranslate = this.nextTranslate;
        this.worker?.postMessage(["translate", lngFrom, lngTo, paragraphs, translateOptions]);
        return nextTranslate;
    }

    private textToHTML(text: string) {
        const div = document.createElement('div');
        div.appendChild(document.createTextNode(text));
        return div.innerHTML;
    }

    private prepareTranslateOptions(paragraphs: string[], isHtml: boolean, isQualityScores: boolean) {
        const translateOptions: translateOptions[] = [];
        paragraphs.forEach(_paragraph => {
          // Each option object can be different for each entry. But to keep the test page simple,
          // we just keep all the options same (specifically avoiding parsing the input to determine
          // html/non-html text)
          translateOptions.push({isQualityScores, isHtml});
        });
        return translateOptions;
      }

    private workerMessageRouter(e: MessageEvent<[string, ...unknown[]]>) {
        if (e.data[0] === "translate_reply" && e.data[1]) {
            const translatedContent = e.data[1] as string[]
            const nt = this.nextTranslate;
            this.nextTranslate = deferred();
            nt.resolve(translatedContent);
            this.lastTranslate = translatedContent;
        } else if (e.data[0] === "load_model_reply" && e.data[1]) {
            const status = e.data[1] as string;
            const ns = this.nextStatus;
            this.nextStatus = deferred();
            ns.resolve(status);
            this.lastStatus = status;
        } else if (e.data[0] === "import_reply" && e.data[1]) {
            const modelRegistry = e.data[1] as ModelRegistry
            this.modelRegistry = modelRegistry;
            console.log("Init Done.")
            this.finishInit();
        }
    }

    private finishInit() {
        // Populate langs
        const modelRegistry = this.modelRegistry as ModelRegistry
        let langsRaw = Object.keys(modelRegistry);
        langsRaw = langsRaw.reduce((acc, key) => acc.concat([key.substring(0, 2), key.substring(2, 2)]), [] as string[])
        const langs = Array.from(new Set(langsRaw));

        const langNames = new Intl.DisplayNames(undefined, {type: "language"});
      
        // Sort languages by display name
        // TODO, figure out why the editor is complainging about this line since it does work.
        langs.sort(
            (a, b) => 
            (langNames.of(a) || "").localeCompare( langNames.of(b) || "")
            );
        this.langOpts = langs;
    }

    public loadModel(fromLang: string, toLang: string) {
        const lngFrom = fromLang;
        const lngTo = toLang;
        if (lngFrom !== lngTo) {
            console.log(`Installing model...`);
            console.log(`Loading model '${lngFrom}${lngTo}'`);
            const nextStat = this.nextStatus;
            this.worker?.postMessage(["load_model", lngFrom, lngTo]);
            this.loadedModels.push(`${lngFrom} ${lngTo}`);
            return nextStat;
        }
    }
}