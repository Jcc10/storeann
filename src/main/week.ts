const weekNoEnd = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
]


export interface DayHours {
    open: [number, number],
    close: [number, number],
    closed?: boolean
}
export interface HolidayHours extends DayHours{
    date: string,
}

export function getDay(
    dayIndex: number,
    startOnSunday: boolean,
    format: "full" | "number"
    ): string | number{
    if(!startOnSunday){
        dayIndex++
    }
    if(format == "number"){
        return dayIndex % 7
    }
    if(format == "full"){
        return weekNoEnd[dayIndex%7];
    }
    return "unreachable on line 17 of getDay";
}