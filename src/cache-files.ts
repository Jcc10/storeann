import {mdlImps} from "./mdl_autogen.ts"
const cssFiles = [
    "/css/style.css",
    "/css/time.css"
]
const pageFiles = [
    "/pages/announcers/closing.html",
    "/pages/announcers/manual.html",
    "/pages/misc/about.html",
    "/pages/misc/help.html",
    "/pages/misc/install.html",
    "/pages/misc/share.html",
    "/pages/settings/hours.html",
    "/pages/settings/speech.html",
    "/pages/settings/store.html",
    "/pages/settings/translation.html",
    "/pages/home.html",
    "/pages/hours.html",
]
const scriptFiles = [
    "/main.js",
    "/mdl.js",
    ...mdlImps,
]
export const filesToCache = [
    "/",
    "/index.html",
    ...cssFiles,
    ...pageFiles,
    ...scriptFiles,
];
export const neverCache = [
    "/hash",
]
export const cacheWipe = [
    ...filesToCache,
    ...neverCache,
]
export const appCacheName = "Store_Ann"
export const modelCacheName = "bergamot-models"