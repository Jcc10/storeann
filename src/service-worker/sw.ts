/// <reference lib="webworker" />
// interface FetchEvent extends Event {
//     request: Request;
//     respondWith: (response: Response | Promise<Response>) => void;
// }
import { filesToCache, neverCache, appCacheName, modelCacheName } from "../cache-files.ts"

let appCache: Cache | null = null;
let modelCache: Cache | null = null;

/* Start the service worker and cache all of the app's content */
// deno-lint-ignore no-explicit-any
self.addEventListener("install", function (event: any) {
    const e = event as ExtendableEvent;
    e.waitUntil(async () => {
        appCache = await caches.open(appCacheName);
        modelCache = await caches.open(modelCacheName);
        await appCache.addAll(filesToCache);
        return;
    });
});

async function unGzipFile(_request: Request, cache: Cache | null, path: URL): Promise<Response> {
    const basePath = path.toString()
    const gzPath = basePath + ".gz";
    console.log(`Requesting Gzip ${gzPath}`);
    let blob: Blob | undefined = await (await cache?.match(gzPath))?.blob();
    if(!blob){
        const baseResp = await fetch(gzPath);
        blob = await baseResp.blob();
        cache?.put(gzPath, new Response(blob));
    }
    const ds = new DecompressionStream('gzip');
    console.log(`Un-Gziping ${gzPath}`);
    const decompressedStream = blob.stream().pipeThrough(ds);
    console.log(`Returning... ${path.toString()}`);
    return await new Response(decompressedStream);
}

function cacheNeverCache(request: Request, cache: Cache | null, path: URL): Response | PromiseLike<Response> {
    for (const file of neverCache) {
        if (path.pathname.startsWith(file)) {
            return fetch(request);
        }
    }
    return cacheOrLive(request, cache);
}

function stratageyChoice(request: Request): Response | PromiseLike<Response> {
    const path = new URL(request.url);
    if(path.pathname.startsWith("/bergamot/models/")){
        return unGzipFile(request, modelCache, path);
    } else {
        return cacheNeverCache(request, appCache, path);
    }
}

async function cacheOrLive(request: Request, cache: Cache | null, creds = true): Promise<Response> {
    let trueRequest = request;
    if (!creds) {
        trueRequest = new Request(request, {
            credentials: "omit", // this is what removes cookies
        });
    }
    let cacheP;
    if (cache == null) {
        cacheP = Promise.reject(undefined);
    } else {
        cacheP = cache?.match(trueRequest);
    }
    const liveP = fetch(trueRequest).catch(()=>Promise.reject());
    const race = await Promise.any([cacheP, liveP]);
    if (race == undefined) {
        return liveP;
    } else {
        return race;
    }
}

/* Serve cached content when offline */
// deno-lint-ignore no-explicit-any
self.addEventListener("fetch", function (event: any) {
    const e = event as FetchEvent;
    const p = stratageyChoice(e.request);
    e.respondWith(p);
});
